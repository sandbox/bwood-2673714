<?php


/**
 * Implementation of hook_form().
 * @return array
 * @internal param $form
 * @internal param $form_state
 */
function apc_cache_admin_form($form, &$form_state) {
  $form = array();
  $form['apc_cache_actions'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Actions'),
    '#description' => t(''),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['apc_cache_actions']['apc_cache_info'] = array(
    '#type' => 'button',
    '#value' => t('View APC Cache Info'),
    '#name' => 'apc_cache_view',
    '#limit_validation_errors' => array(),
    '#submit' => array('apc_cache_actions_submit'),
    '#executes_submit_callback' => TRUE,
    '#prefix' => '<div class="apc-cache-admin-form">',
    '#suffix' => '</div><br />',
  );
  $form['apc_cache_actions']['apc_cache_clear'] = array(
    '#type' => 'button',
    '#value' => t('Clear APC Cache'),
    '#name' => 'apc_cache_clear',
    '#limit_validation_errors' => array(),
    '#submit' => array('apc_cache_actions_submit'),
    '#executes_submit_callback' => TRUE,
    '#prefix' => '<div class="apc-cache-admin-form">',
    '#suffix' => '</div><br />',
  );
  $form['apc_cache_settings'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Settings'),
    '#description' => t("You must choose at least one cache."),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['apc_cache_settings']['apc_cache_system_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear/show the APC system (filehits) cache.'),
    '#description' => t(''),
    '#default_value' => variable_get('apc_cache_system_cache', FALSE),
  );
  $form['apc_cache_settings']['apc_cache_user_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear/show the APC user cache.'),
    '#description' => t(''),
    '#default_value' => variable_get('apc_cache_user_cache', FALSE),
  );
  $form['apc_cache_settings']['apc_cache_settings_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('apc_cache_settings_submit'),
    '#validate' => array('apc_cache_settings_validate'),
  );

  return $form;
}

/**
 * Form submit handler.
 * Handle settings from the "actions" fieldset.
 *
 * @param $form_state
 * @internal param $form
 */
function apc_cache_actions_submit($form, &$form_state) {

  $secret = variable_get('apc_cache_secret', '');
  if (empty($secret)) {
    drupal_set_message('Cannot find the secret token.  (Try disabling and re-enabling this module.)');
    return;
  }

  if (isset($form_state['values']['apc_cache_actions']['apc_cache_info'])) {
    drupal_goto('apc-cache-info', array('query' => array('s' => $secret)));
  }
  elseif (isset($form_state['values']['apc_cache_actions']['apc_cache_clear'])) {
    drupal_goto('apc-cache-clear', array('query' => array('s' => $secret)));
  }
}

/**
 * Form validation handler.
 *
 * @param $form
 * @param $form_state
 * @return bool
 */
function apc_cache_settings_validate($form, &$form_state) {
  if ($form_state['values']['apc_cache_settings']['apc_cache_system_cache'] == FALSE && $form_state['values']['apc_cache_settings']['apc_cache_user_cache'] == FALSE) {
    form_set_error('apc_cache_settings', t("This module is useless unless at least one cache is selected."));
    return FALSE;
  }
  return TRUE;
}

/**
 * Form submit handler.
 * Handle settings from the "settings" fieldset.
 *
 * @param $form
 * @param $form_state
 */
function apc_cache_settings_submit($form, &$form_state) {
  variable_set('apc_cache_system_cache', $form_state['values']['apc_cache_settings']['apc_cache_system_cache']);
  variable_set('apc_cache_user_cache', $form_state['values']['apc_cache_settings']['apc_cache_user_cache']);
}