<?php

/**
 *
 */
define('APC_CACHE_NO_RESULTS', "No APC information returned. At least one cache must be selected. Check 'drush variable-get \"apc_cache\"' or check the configure page for this module.");

/**
 * Implementation of hook_drush_command().
 *
 */
function apc_cache_drush_command() {
  $items = array();


  $items['apc-cache-clear'] = array(
    'aliases' => array('acc', 'apc-cc'),
    'description' => dt("Clear the APC caches in accordance with the module\'s configuration."),
    'callback' => 'drush_apc_cache_clear_caches',
  );
  $items['apc-cache-info'] = array(
    'aliases' => array('aci', 'apc-info'),
    'description' => dt('View cache metadata for the caches selected in the module\'s configuration.'),
    'callback' => 'drush_apc_cache_info',
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function apc_cache_drush_help($section) {
  switch ($section) {
    case 'drush:apc-cache-clear':
      return dt("Clear the APC caches in accordance with the module\'s configuration.");
    case 'drush:apc-cache-info':
      return dt('View cache metadata for the caches selected in the module\'s configuration.');
  }
}


/**
 * Command callback for clearning the APC caches.
 * Displays changes detected in apc-cache-info() output
 */
function drush_apc_cache_clear_caches() {
  $url = apc_cache_get_url('apc-cache-clear');
  if (!$url) {
    return;
  }

  $data = apc_cache_make_request($url);
  if (empty($data)) {
    drupal_set_message(dt("Request returned no data."), 'error');
    return;
  }

  if (!isset($data['results'])) {
    $error_message = dt(APC_CACHE_NO_RESULTS);
    drush_set_error('apc_cache_no_info', $error_message);
    return;
  }
  foreach ($data['results'] as $type => $items) {

    if ($items['return'] != 0) {
      // Clearing this cache type failed.  Don't include stats.
      drupal_set_message($type['message'], 'error');
      continue;
    }
    drupal_set_message($items['message'], 'status');
    $table = array();
    $diff = array_diff_assoc($data['stats']['before'][$type], $data['stats']['after'][$type]);
    if (count($diff)) {
      print "\n";
      drush_print(dt("Cache clear produced these differences in the @type cache.", array("@type" => $type)));
      foreach (array_keys($diff) as $key) {
        $table[$key] = $data['stats']['before'][$type][$key] . "  -->  " . $data['stats']['after'][$type][$key];
      }
      drush_print_table(drush_key_value_to_array_table($table));
    }
    else {
      drush_print(dt("Cache clear produced no differences in the @type cache.", array("@type" => $type)));
    }
    print "\n";
  }

}

/**
 * Command callback for displying apc-cache-info() output.
 */
function drush_apc_cache_info() {
  $url = apc_cache_get_url('apc-cache-info');
  $data = apc_cache_make_request($url);

  if (array_key_exists('apc_cache_status', $data)) {
    //TODO: drush print table would be nice
    drush_print("\n" . $data['apc_cache_status']);
  }
  else {
    $error_message = dt(APC_CACHE_NO_RESULTS);
    drush_set_error('apc_cache_no_info', $error_message);
    return;
  }
}


/**
 * Construct url with secret token for drupal_http_request().
 *
 * @param $path
 * @return string
 */
function apc_cache_get_url($path) {
  global $base_url;
  $secret = variable_get('apc_cache_secret', '');
  if (empty($secret)) {
    return drush_set_error('apc_cache_missing_token', dt("Drush does not have access to the secret token required to clear the APC cache."));
  }
  //TODO ensure menu item exists?
  return $base_url . "/$path?s=" . urlencode($secret);
}

/**
 * Wrapper for drupal_http_request().
 *
 * @param $url
 * @return mixed
 */
function apc_cache_make_request($url) {
  $response = drupal_http_request($url, array());
  //todo check status code and that response->data isn't null
  $data = json_decode($response->data, TRUE);
  return $data;
}